#ifndef _UwUnet_
#define _UwUnet_

void *get_in_addr(struct sockaddr *sa){
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int send_data(int fd, char *header, char *content_type, char *body, int length_file)
{
    char *response = malloc(strlen(header) + strlen(content_type) + length_file + 1);
    printf("%s\n", header);

    // Build HTTP response and store it in response
    int hlen = snprintf(response, strlen(header) + strlen(content_type) + strlen("\n\n\n"), "%s\n%s\n\n", header, content_type);

    strncat(response, body, strlen(body));

    // sending it baeee
    int rv;
    rv = write(fd, response, hlen + length_file);

    if (rv < 0) {
        perror("send");
    }

    return 0;
}

//int get_location(char string, char serverroot, char *filename){
//	sprintf(&filename, "%s%s", serverroot, )
//}

#endif
