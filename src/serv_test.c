#include <sys/socket.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
//#include "UwUnet.h"

void *printhello(void *t){
	while(1){
		printf("hello\n");
		
		sleep(2);	
	}
}

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char *argv[]){
	pthread_t thread_hello;

	int t = 2;
	int rc;
	rc = pthread_create(&thread_hello, NULL, printhello, NULL);

	struct sockaddr_storage their_addr;
	fflush(stdout);
	int listenfd = 0, connfd = 0;
	struct sockaddr_in serv_addr;

	char sendBuff[1025];
	time_t ticks;

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	
	memset(&serv_addr, '0', sizeof(serv_addr));
	memset(sendBuff, '0', sizeof(sendBuff));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(5000);

	bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	printf("listening on port 5000\n");
	listen(listenfd, 10);
	char s[INET6_ADDRSTRLEN];

	while(1)
	{
		socklen_t sin_size = sizeof their_addr;
		connfd = accept(listenfd, (struct sockaddr*)&their_addr, &sin_size);

		char buffer_recv[4096];
		recv(connfd, &buffer_recv, sizeof(buffer_recv), 0);
		printf("%s\n", buffer_recv);

		inet_ntop(their_addr.ss_family,
            get_in_addr((struct sockaddr *)&their_addr),
            s, sizeof s);
		printf("accepted connection from %s\n", s);

		ticks = time(NULL);
		snprintf(sendBuff, sizeof(sendBuff), "%.24s\r\n", ctime(&ticks));
		write(connfd, sendBuff, strlen(sendBuff));

		close(connfd);
		sleep(1);
	}
}